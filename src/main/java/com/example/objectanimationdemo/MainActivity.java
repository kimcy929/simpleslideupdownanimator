package com.example.objectanimationdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private FrameLayout viewAnim;
    private Button btnTopLeft;
    private Button btnTopRight;
    private Button btnBottomLeft;
    private Button btnBottomRight;

    /*private final int TOP = 0;
    private final int BOTTOM = 1;

    private int currentPosition = R.id.btnBottomLeft;

    private int oldPosition = BOTTOM;
    private int newPosition = BOTTOM;

    private final long ANIMATION_DURATION = 3000L;

    private int gravity;*/

    private SideEdgeAnimatorUtils sideEdgeAnimatorUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
    }

    private void initView() {
        viewAnim = (FrameLayout) findViewById(R.id.viewAnim);
        btnTopLeft = (Button) findViewById(R.id.btnTopLeft);
        btnTopRight = (Button) findViewById(R.id.btnTopRight);
        btnBottomLeft = (Button) findViewById(R.id.btnBottomLeft);
        btnBottomRight = (Button) findViewById(R.id.btnBottomRight);

        btnTopLeft.setOnClickListener(onClickListener);

        btnTopRight.setOnClickListener(onClickListener);

        btnBottomLeft.setOnClickListener(onClickListener);

        btnBottomRight.setOnClickListener(onClickListener);

        sideEdgeAnimatorUtils = new SideEdgeAnimatorUtils(viewAnim);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {

        @Override
        public void onClick(final View view) {
            int id = view.getId();

            switch (id) {
                case R.id.btnTopLeft:
                    sideEdgeAnimatorUtils.startAnimation(SideEdgeAnimatorUtils.TOP_LEFT);
                    break;

                case R.id.btnTopRight:
                    sideEdgeAnimatorUtils.startAnimation(SideEdgeAnimatorUtils.TOP_RIGHT);
                    break;

                case R.id.btnBottomLeft:
                    sideEdgeAnimatorUtils.startAnimation(SideEdgeAnimatorUtils.BOTTOM_LEFT);
                    break;

                case R.id.btnBottomRight:
                    sideEdgeAnimatorUtils.startAnimation(SideEdgeAnimatorUtils.BOTTOM_RIGHT);
                    break;

            }

            /*if (id == currentPosition) return;

            final float translationByY;
            if (currentPosition == R.id.btnTopLeft || currentPosition == R.id.btnTopRight) {
                translationByY = -viewAnim.getHeight();
            } else {
                translationByY = viewAnim.getHeight();
            }

            switch (id) {
                case R.id.btnTopLeft:
                    currentPosition = R.id.btnTopLeft;
                    gravity = Gravity.TOP;
                    newPosition = TOP;
                    break;

                case R.id.btnTopRight:
                    currentPosition = R.id.btnTopRight;
                    gravity = Gravity.TOP | Gravity.END;
                    newPosition = TOP;
                    break;

                case R.id.btnBottomLeft:
                    currentPosition = R.id.btnBottomLeft;
                    gravity = Gravity.BOTTOM;
                    newPosition = BOTTOM;
                    break;

                case R.id.btnBottomRight:
                    currentPosition = R.id.btnBottomRight;
                    gravity = Gravity.BOTTOM | Gravity.END;
                    newPosition = BOTTOM;
                    break;

            }

            viewAnim.animate().cancel();

            viewAnim.animate()
                    .translationY(translationByY)
                    .setDuration(ANIMATION_DURATION)
                    .setInterpolator(new FastOutSlowInInterpolator())
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            animation.cancel();

                            viewAnim.setAlpha(0f);
                            ViewPropertyAnimator viewPropertyAnimator = viewAnim.animate();
                            if (oldPosition != newPosition) {
                                viewPropertyAnimator
                                       //.alpha(0)
                                        .translationY(-translationByY);//top -> bottom and otherwise
                                oldPosition = newPosition;
                            } else {
                                viewPropertyAnimator.translationX(0);// left -> right and otherwise

                            }
                            viewPropertyAnimator
                                    .setDuration(0)
                                    .setStartDelay(0)
                                    .setListener(new AnimatorListenerAdapter() {
                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                            viewAnim.setAlpha(1f);
                                            animation.cancel();

                                            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(300, 100);
                                            params.gravity = gravity;
                                            viewAnim.setLayoutParams(params);

                                            viewAnim.animate()
                                                    .translationY(0)
                                                    .translationX(0)
                                                    .setDuration(ANIMATION_DURATION)
                                                    .setInterpolator(new FastOutSlowInInterpolator())
                                                    .setListener(new AnimatorListenerAdapter() {
                                                        @Override
                                                        public void onAnimationEnd(Animator animation) {
                                                            animation.cancel();
                                                        }
                                                    });
                                        }
                                    });
                        }
                    });*/

        }
    };
}
