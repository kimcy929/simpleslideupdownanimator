package com.example.objectanimationdemo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.view.Gravity;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.FrameLayout;

/**
 * Created by kimcy929 on 7/3/2017.
 */

public class SideEdgeAnimatorUtils {
    private View viewAnim;

    private final int TOP = 0;
    private final int BOTTOM = 1;

    private int oldPosition = BOTTOM;
    private int newPosition = BOTTOM;

    public static final int TOP_LEFT = 0;
    public static final int TOP_RIGHT = 1;
    public static final int BOTTOM_LEFT = 2;
    public static final int BOTTOM_RIGHT = 3;

    private int currentPosition = BOTTOM_LEFT;

    private final long ANIMATION_DURATION = 200L;

    private FastOutSlowInInterpolator interpolator = new FastOutSlowInInterpolator();

    private int gravity;

    private float translationByY;

    public SideEdgeAnimatorUtils(View targetView) {
        this.viewAnim = targetView;
    }

    public void startAnimation(int position) {
        if (position == currentPosition) return;

        if (currentPosition == TOP_LEFT || currentPosition == TOP_RIGHT) {
            translationByY = -viewAnim.getHeight();
        } else {
            translationByY = viewAnim.getHeight();
        }

        switch (position) {
            case TOP_LEFT:
                currentPosition = TOP_LEFT;
                gravity = Gravity.TOP;
                newPosition = TOP;
                break;

            case TOP_RIGHT:
                currentPosition = TOP_RIGHT;
                gravity = Gravity.TOP | Gravity.END;
                newPosition = TOP;
                break;

            case BOTTOM_LEFT:
                currentPosition = BOTTOM_LEFT;
                gravity = Gravity.BOTTOM;
                newPosition = BOTTOM;
                break;

            case BOTTOM_RIGHT:
                currentPosition = BOTTOM_RIGHT;
                gravity = Gravity.BOTTOM | Gravity.END;
                newPosition = BOTTOM;
                break;

        }

        viewAnim.animate().cancel();

        viewAnim.animate()
                .translationY(translationByY)
                .setDuration(ANIMATION_DURATION)
                .setInterpolator(interpolator)
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        viewAnim.setAlpha(0f);
                        ViewPropertyAnimator viewPropertyAnimator = viewAnim.animate();
                        if (oldPosition != newPosition) {
                            viewPropertyAnimator.translationY(-translationByY); // top -> bottom and otherwise
                            oldPosition = newPosition;
                        } else {
                            viewPropertyAnimator.translationX(0); // left -> right and otherwise
                        }
                        viewPropertyAnimator
                                .setDuration(0)
                                .setStartDelay(0)
                                .withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        viewAnim.setAlpha(1f);

                                        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) viewAnim.getLayoutParams();
                                        params.gravity = gravity;
                                        viewAnim.setLayoutParams(params);

                                        viewAnim.animate()
                                                .translationY(0)
                                                .translationX(0)
                                                .setDuration(ANIMATION_DURATION)
                                                .setInterpolator(interpolator)
                                                .setListener(new AnimatorListenerAdapter() {
                                                    @Override
                                                    public void onAnimationEnd(Animator animation) {
                                                        animation.cancel();
                                                    }
                                                });
                                    }
                                });
                    }
                });
    }
}
